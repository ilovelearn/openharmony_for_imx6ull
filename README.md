# IMX6ULL鸿蒙系统
> 支持IMX6ULL 系列开发板的鸿蒙系统补丁

## 介绍

### 软件架构


## 配置开发环境
> 使用我们的一键配置环境脚本在ubuntu-18.04系统上一键配置鸿蒙系统运行环境
```
book@100ask:~$  wget --no-check-certificate -O Configuring_ubuntu.sh https://weidongshan.coding.net/p/DevelopmentEnvConf/d/DevelopmentEnvConf/git/raw/master/Configuring_ubuntu.sh && sudo chmod +x Configuring_ubuntu.sh && sudo ./Configuring_ubuntu.sh
```

## 编译鸿蒙系统

### 配置repo工具

> 首先需要设置GIT用户名和密码

```
book@100ask:~$ git config --global user.name "100ask"
book@100ask:~$ git config --global user.email "weidongshan@qq.com"
book@100ask:~$ git config --global credential.helper store
```

> 获取并安装repo工具

```
book@100ask:~$ curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
book@100ask:~$ sudo cp repo  /usr/local/bin/repo && sudo chmod a+x /usr/local/bin/repo
book@100ask:~$ sudo pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
```

### 同步源码

```
book@100ask:~$ cd /home/book
book@100ask:~$ mkdir  openharmony && cd  openharmony
book@100ask:~$ repo init -u https://gitee.com/openharmony/manifest.git -b OpenHarmony-1.0
book@100ask:~$ repo sync -c -j8

```

### 打IMX6ULL系统补丁
> 获取补丁文件
```
book@100ask:~$ cd  /home/book
book@100ask:~$ git  clone  https://gitee.com/weidongshan/openharmony_for_imx6ull.git
```
> 给鸿蒙系统打补丁
```
book@100ask:~$ cd  /home/book/openharmony
book@100ask:~$ patch -p1 < /home/book/openharmony_for_imx6ull/patch/hmos_v1.0_imx6ull.patch
```
### 编译IMX6ULL内核源码
```
book@100ask:~$ cd  /home/book/openharmony/kernel/liteos_a
book@100ask:~$ cp  tools/build/config/debug/imx6ull_clang.config .config   // 配置
book@100ask:~$ make clean      // 前面为hi3518ev300编译过，先清除一下
book@100ask:~$ make  -j  8     // 编译内核，可以得到out/imx6ull/liteos.bin
book@100ask:~$ make  rootfs    // 编译根文件系统，可以得到rootfs.img
book@100ask:~$ cp  out/imx6ull/rootfs.img out/imx6ull/rootfs.jffs2 // 改个名易辨认，烧写工具使用rootfs.jffs2
```
## 烧写运行鸿蒙系统
> 使用百问网独家烧写工具点击鸿蒙按钮进行烧写

## 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request